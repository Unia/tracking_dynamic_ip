﻿#include "pch.h"
#pragma comment(lib, "ws2_32.lib")
#define BUFF_SIZE 10000

using namespace std;

const string GetIP();
void GetWebsite(const string url, const string num, const string ipNum);

int main(int argc, char** argv)
{
	if (argc > 1)
	{
		GetWebsite("kphk42.dothome.co.kr", argv[1], GetIP());
	}
	else
	{
		cout << "인수를 추가하여 주세요" << endl;
	}
}

const string GetIP()
{
	WSADATA wsaData;
	SOCKET ipSocket;

	struct sockaddr_in sockaddr_info1, sockaddr_info2;
	int sockaddr_info_size = sizeof(struct sockaddr_in);

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		cout << "WSAStartup failed.\n";
		system("pause");
	}
	sockaddr_info1.sin_family = AF_INET;
	sockaddr_info1.sin_port = htons(4567);
	sockaddr_info1.sin_addr.s_addr = inet_addr("8.8.8.8");
	ipSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	//연결해서 내 아이피를 보는 작업
	connect(ipSocket, (struct sockaddr*)&sockaddr_info1, sizeof sockaddr_info1);
	getsockname(ipSocket, (struct sockaddr*)&sockaddr_info2, &sockaddr_info_size);
	
	string ip = inet_ntoa(sockaddr_info2.sin_addr);

	closesocket(ipSocket);
	WSACleanup();
	return ip;
}

void GetWebsite(const string url, const string num, const string ipNum)
{
	cout << "작업 웹사이트에 아이피 기재." << endl;
	cout << "num = " << num << endl;
	cout << "ipNum = " << ipNum << endl;

	WSADATA wsaData;
	SOCKET webSocket;
	SOCKADDR_IN sockAddr;
	int lineCount = 0;
	int rowCount = 0;
	struct hostent *host;
	string *getHttp = new string();
	getHttp->reserve(112);
	getHttp->append("GET /IpChecking/UpdateIp.php?num=");
	getHttp->append(num);
	getHttp->append("&ip='");
	getHttp->append(ipNum);
	getHttp->append("' HTTP/1.1\r\nHost: ");
	getHttp->append(url);
	getHttp->append("\r\nConnection: close\r\n\r\n");

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		cout << "WSAStartup failed.\n";
		system("pause");
	}

	webSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	host = gethostbyname(url.c_str());

	sockAddr.sin_port = htons(80);
	sockAddr.sin_family = AF_INET;
	sockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);

	if (connect(webSocket, (SOCKADDR*)(&sockAddr), sizeof(sockAddr)) != 0) {
		cout << "Could not connect";
		system("pause");
	}
	send(webSocket, getHttp->c_str(), strlen(getHttp->c_str()), 0);

	string response;
	char buffer[BUFF_SIZE];
	int dataLength;
	while ((dataLength = recv(webSocket, buffer, BUFF_SIZE, 0)) > 0) {
		int i = 0;
		while (buffer[i] >= 32 || buffer[i] == '\n' || buffer[i] == '\r') {
			response += buffer[i];
			i += 1;
		}
	}
	cout << response << endl;

	closesocket(webSocket);
	WSACleanup();
	cout << "작업 완료 하였습니다." << endl;
}